﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Stel.Desktop.src.utils;

namespace System
{
    public static class SCREEN
    {
        public const int WIDTH = 1024;
        public const int HEIGHT = 576;
    }
}

namespace Stel.Desktop
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;

        ContextHandler context;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = SCREEN.HEIGHT;
            graphics.PreferredBackBufferWidth = SCREEN.WIDTH;
            graphics.IsFullScreen = false;

            Content.RootDirectory = "Content";
            
        }


        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            context = new ContextHandler(Content, GraphicsDevice);
            this.IsMouseVisible = true;
            base.Initialize();
            this.IsFixedTimeStep = true;

        }

        protected override void LoadContent()
        {
            
        }


        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            GamePadState gp = GamePad.GetState(PlayerIndex.One);
            KeyboardState kb = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            context.update(gp,kb,ms);

            if (context.mode == "exit" || kb.IsKeyDown(Keys.Escape))
                Exit();

            base.Update(gameTime);
        }

        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            context.draw();

            base.Draw(gameTime);
        }
    }
}
