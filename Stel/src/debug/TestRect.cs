﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Stel.Desktop.src.debug
{
    class TestRect
    {
        Texture2D sprite;
        Rectangle boundingBox;
        Vector2 pos;
        Vector2 vel;

        public TestRect() { }
        public TestRect(Texture2D tex, Vector2 position)
        {
            this.sprite = tex;
            this.pos = position;
            this.boundingBox = new Rectangle(position.ToPoint(), new Point(sprite.Width, sprite.Height));
            this.vel = Vector2.Zero;
        }
        public Vector2 getPos() { return this.pos; }
        public void setPos(Vector2 pos) {  this.pos = pos; }
        public Vector2 getVel() { return this.vel; }
        public void setVel(Vector2 vel) { this.vel = vel; }
        public Texture2D getSprite() { return this.sprite; }
        public Rectangle getRect() { return this.boundingBox; }
    }
}
