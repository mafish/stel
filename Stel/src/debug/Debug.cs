﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace System
{
    public class Debug
    {
        Texture2D consoleSprite;
        SpriteFont consoleFont;
        Rectangle consoleRect;
        bool consoleIsOn;
        static string consoleText;

        public Debug(ContentManager c, Rectangle conRect)
        {
            this.consoleIsOn = true;
            consoleText = "Stel v0.1";
            this.consoleSprite = c.Load<Texture2D>("blank");
            this.consoleFont = c.Load<SpriteFont>("debug");
            this.consoleRect = conRect;
        }

        public void Toggle()
        {
            this.consoleIsOn = !this.consoleIsOn;
        }


        public static void DebugMsg(string msg)
        {
            if (msg.Length > 20)
                msg.Insert(20, "\n");
            consoleText += msg;
            consoleText += "\n";
            int numLines = consoleText.Split('\n').Length;
            if (numLines > 10)
            {
                int index = consoleText.IndexOf(System.Environment.NewLine);
                consoleText = consoleText.Substring(index + System.Environment.NewLine.Length);
            }
        }

        public void Draw(SpriteBatch s)
        {
            if (this.consoleIsOn)
            {
                s.Draw(this.consoleSprite, this.consoleRect, new Color(255, 255, 255, 50));
                s.DrawString(this.consoleFont, consoleText, this.consoleRect.Location.ToVector2(), Color.White);
            }
        }
    }

}
