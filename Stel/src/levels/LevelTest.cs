﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Stel.Desktop.src.objects;
using Stel.Desktop.src.utils;

namespace Stel.Desktop.src.levels
{
    class LevelTest
    {
        Player p1;
        Player dummy;

        List<Lamp> streetLamps;
        LightScene lightScene;


        FloorContainer fc;
        Background bg;

        Camera camera2D;
        static int mouseWheel = 0; 

        public LevelTest(ContentManager con, GraphicsDevice gd)
        {
            dummy = new Player(con, new Point(450,300));
            p1 = new Player(con, new Point(400, 300));
            fc = new FloorContainer(con, new Point(SCREEN.WIDTH, SCREEN.HEIGHT), "street", "levelTest");
            camera2D = new Camera(gd.Viewport,new Point(300,300));
            bg = new Background(con, "street");

            this.streetLamps = new List<Lamp>();
            for(int i = 0; i < 10; i++)
            {
               this.streetLamps.Add(new Lamp(con, new Rectangle(i * 300, 400, 100, 200)));
            }

            this.lightScene = new LightScene(con);
        }
        public void Input(KeyboardState kb, MouseState ms)
        {
            if(ms.ScrollWheelValue != mouseWheel)
            {
                if (ms.ScrollWheelValue > mouseWheel)
                    camera2D.Zoom += 0.05f;
                else
                    camera2D.Zoom -= 0.05f;
                mouseWheel = ms.ScrollWheelValue;
            }
            if(ms.LeftButton == ButtonState.Pressed)
                lightScene.DarknessLevel = lightScene.DarknessLevel + 0.002f;
            else
                lightScene.DarknessLevel = lightScene.DarknessLevel - 0.002f;
            p1.Input(kb, ms);
        }
        public void Update()
        {
            //append all objects that possibly can collide with anything
            List<Collider> coliderList = new List<Collider>();
            foreach (var o in fc.Floors)
                coliderList.Add(o);
            coliderList.Add(dummy);

            dummy.Update();
            dummy.CheckCollision(coliderList);

            p1.Update();
            p1.CheckCollision(coliderList);

            fc.Update(p1.BoundingBox.Center);

            bg.Update(camera2D.CameraPosition);

            camera2D.Follow(p1);

            foreach (var sl in this.streetLamps)
                sl.Update(this.lightScene.DarknessLevel);
        }
        public void Draw(SpriteBatch s)
        {
            //Create list of all light objects
            List<LightSource> lightSourceList = new List<LightSource>();    
            foreach (var o in this.streetLamps)
                lightSourceList.Add(o);
            lightSourceList.Add(p1.flashlight);

            //Create light mask - black screen with transparent empty spaces
            //This CLEARS spritebatch buffer so has to be called before anything else is drew
            lightScene.UpdateLights(s, lightSourceList, camera2D.Transform, camera2D.CameraPosition);

            //draw all components with camera transformation in mind
            s.End();
            s.Begin(transformMatrix: camera2D.Transform);
            bg.Draw(s);
            p1.Draw(s);
            dummy.Draw(s);
            fc.Draw(s,p1.Pos);

            foreach (var l in this.streetLamps)
                l.Draw(s);

            //after drwaing cll sprites, draw lightScene made with UpdateLights method
            //needs to be called without transformationMatrix, because it was already transformed in UpdateLights method
            s.End();
            s.Begin();
            lightScene.Draw(s);
        }
    }
}
