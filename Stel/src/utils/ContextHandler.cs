﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Stel.Desktop.src.menu;

using Stel.Desktop;

namespace Stel.Desktop.src.utils
{
    class ContextHandler
    {
        public string mode = "intro";

        public SpriteBatch introSpriteBatch;
        public SpriteBatch menuSpriteBatch;
        public SpriteBatch mainSpriteBatch;
        public SpriteBatch levelSpriteBatch;

        public Menu menu;
        public Intro intro;
        public levels.LevelTest levelTest;

        private Debug debug;

        public ContextHandler(ContentManager con, GraphicsDevice gd)
        {
            this.mode = "menu";
            this.introSpriteBatch = new SpriteBatch(gd);
            this.mainSpriteBatch = new SpriteBatch(gd);
            this.menuSpriteBatch = new SpriteBatch(gd);
            this.levelSpriteBatch = new SpriteBatch(gd);

            debug = new Debug(con, new Rectangle(30, 30, 200, 300));

            this.menu = new Menu(con, new Vector2(100, 100), new Vector2(250, 100), new Vector2(400, 100));
            this.intro = new Intro(con, new Point(SCREEN.WIDTH, SCREEN.HEIGHT));
            this.levelTest = new levels.LevelTest(con, gd);

        }

        public void input(KeyboardState kb)
        {
            if(kb.IsKeyDown(Keys.OemTilde))
            {
                debug.Toggle();
            }
        }

        public void update(GamePadState gp, KeyboardState kb, MouseState ms)
        {
            input(kb);
            if(this.mode == "intro")
            {

            }
            else if (this.mode == "menu")
            {
                if (ms.LeftButton == ButtonState.Pressed)
                    this.mode = this.menu.CheckClicks(ms.Position);
            }
            else if(this.mode == "level")
            {
                this.levelTest.Input(kb,ms);
                this.levelTest.Update();

            }
        }
        public void draw()
        {
            if(this.mode == "intro")
            {
                this.introSpriteBatch.Begin();
                this.intro.draw(this.introSpriteBatch);
                if (this.intro.transparency > 0)
                    this.intro.transparency -= 0.01f;
                else
                    this.mode = "menu";
                this.introSpriteBatch.End();
            }
            if (this.mode == "menu")
            {
                this.menuSpriteBatch.Begin();
                this.menu.Draw(this.menuSpriteBatch);
                this.menuSpriteBatch.End();
            }
            else if (this.mode == "level")
            {
                this.levelSpriteBatch.Begin();
                this.levelTest.Draw(this.levelSpriteBatch);
                this.levelSpriteBatch.End();
            }

            this.mainSpriteBatch.Begin();
            this.debug.Draw(mainSpriteBatch);    
            this.mainSpriteBatch.End();

        }


    }
}
