﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Stel.Desktop.src.objects;
using Stel.Desktop.src.debug;

namespace Stel.Desktop.src.utils
{
    public class LightScene
    {
        Texture2D spriteBlank;
        public Texture2D spriteLight;
        RenderTarget2D target;
        float darknessLevel = 0f;

        public float DarknessLevel { get => darknessLevel; set => darknessLevel = Math.Min(Math.Max(value, 0f), 1f); }

        public LightScene(ContentManager c)
        {
            this.spriteBlank = c.Load<Texture2D>("blank");
            this.spriteLight = this.spriteBlank;
        }
        public void UpdateLights(SpriteBatch s, List<LightSource> l, Matrix transform, Point center)
        {
            if(target == null)
                target = new RenderTarget2D(s.GraphicsDevice, SCREEN.WIDTH, SCREEN.HEIGHT);

            BlendState bsSubstracy = new BlendState
            {
                ColorSourceBlend = Blend.SourceAlpha,
                ColorDestinationBlend = Blend.One,
                ColorBlendFunction = BlendFunction.Add,

                AlphaSourceBlend = Blend.SourceAlpha,
                AlphaDestinationBlend = Blend.One,
                AlphaBlendFunction = BlendFunction.ReverseSubtract
            };


            s.End();
            s.GraphicsDevice.SetRenderTarget(target);
            s.Begin(SpriteSortMode.Deferred, bsSubstracy, transformMatrix: transform);
            s.Draw(this.spriteBlank, new Rectangle(center.X - SCREEN.WIDTH / 2, center.Y - SCREEN.HEIGHT / 2, SCREEN.WIDTH, SCREEN.HEIGHT), new Color(155, 150, 255, Convert.ToInt32((1f - this.darknessLevel) * 255) ));
            foreach (var ls in l)
                if (ls.isOn)
                    s.Draw(ls.lightSprite, ls.lightRect.Location.ToVector2() , null,  new Color(ls.tint, ls.brightness), ls.rotation, ls.origin, ls.scale, SpriteEffects.None, 1f);
            s.End();

            s.GraphicsDevice.SetRenderTarget(null);

            s.Begin();

            this.spriteLight = target;
        }
        public void Draw(SpriteBatch s)
        {
            s.Draw(this.spriteLight, new Vector2(0, 0), Color.White);
        }
    }
}
