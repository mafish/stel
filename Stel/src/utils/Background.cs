﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Stel.Desktop.src.utils
{
    public class Background
    {
        Texture2D farFarSprite;
        Rectangle farFarRect;
        Texture2D farSprite;
        Rectangle farRect;

        Texture2D closeSrite;
        Rectangle[] closeRect;

        public Background(ContentManager c, string biome)
        {
            this.farFarSprite = c.Load<Texture2D>("wideBackground");
            this.farFarRect.Location = new Point(0, 0);
            this.farFarRect.Size = farFarSprite.Bounds.Size;
            this.farFarRect.Height *= 2;

            this.farSprite = c.Load<Texture2D>("wideBackground2");
            this.farRect.Location = new Point(0, 0);
            this.farRect.Size = farSprite.Bounds.Size;

            this.closeSrite = c.Load<Texture2D>("wideBackground3");
            closeRect = new Rectangle[5];
            for(int i = 0; i < closeRect.Length; i++)
            {            
                closeRect[i].Location = new Point(i * closeSrite.Width, 100);
                closeRect[i].Size = closeSrite.Bounds.Size;
            }

        }
        public void Update(Point screenCenter)
        {
            this.farFarRect.Location = new Vector2(screenCenter.X * 0.95f - (SCREEN.WIDTH * 0.65f), this.farFarRect.Location.Y).ToPoint();
            this.farRect.Location = new Vector2(screenCenter.X * 0.85f - (SCREEN.WIDTH * 0.65f), this.farFarRect.Location.Y).ToPoint();
        }
        public void Draw(SpriteBatch s)
        {
            s.Draw(farFarSprite, farFarRect, Color.White);
            s.Draw(farSprite, farRect, new Color(100, 100, 100, 240));

            for (int i = 0; i < closeRect.Length; i++)
            {
                s.Draw(closeSrite, closeRect[i], new Color(100, 100, 100, 255));
            }
        }
    }
}
