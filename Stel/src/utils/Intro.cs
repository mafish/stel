﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Stel.Desktop.src.utils
{
    class Intro
    {
        public Texture2D sprite;
        public Point size;
        public float transparency = 1.0f;

        public Intro(ContentManager c, Point dims)
        {
            this.sprite = c.Load<Texture2D>("blank");
            this.size = dims;
            transparency = 1.0f;
        }
        public void draw(SpriteBatch s)
        {
            s.Draw(this.sprite, new Rectangle(new Point(0, 0), this.size), Color.Black * this.transparency);
        }
    }
}
