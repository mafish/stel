﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Stel.Desktop;

using Stel.Desktop.src.objects;

namespace Stel.Desktop.src.utils
{
    public class Camera
    {
        public Matrix Transform { get; private set; }
        public float RotZ { get => rotZ; set => rotZ = value; }
        public float Zoom { get => zoom; set => zoom = value; }
        public Point CameraPosition { get => cameraPosition; set => cameraPosition = value; }

        public Viewport vp;
        private float rotZ = 0.0f;
        private float zoom = 1.0f;

        private Point cameraPosition = new Point(0, 0);

        public Camera(Viewport newVp, Point cPos)
        {
            this.vp = newVp;
            this.CameraPosition = cPos;
        }

        public void Follow(ForegroundObject target)
        {
            if (this.zoom <= 0.1f)
                this.zoom = 0.1f;
            this.CameraPosition += (( (target.boundingBox.Center + new Point(0,-150) )-
                            this.CameraPosition).ToVector2() * 
                            new Vector2(0.15f, 0.025f)).ToPoint();
            var center = Matrix.CreateTranslation(new Vector3(-CameraPosition.X, -CameraPosition.Y, 0));
            var rot = Matrix.CreateRotationZ(this.RotZ);
            var scale = Matrix.CreateScale(new Vector3(this.Zoom, this.Zoom, 0));
            var translat = Matrix.CreateTranslation(new Vector3(this.vp.Width / 2, this.vp.Height / 2, 0));

            Transform = center * rot * scale * translat;
        }
    }
}
