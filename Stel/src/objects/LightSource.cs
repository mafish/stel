﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Stel.Desktop.src.objects
{
    public class LightSource
    {
        public Texture2D lightSprite;
        public Rectangle lightRect;
        public bool isOn = false;
        public Color tint = new Color(0,0,0, 0);
        public float brightness = 0.75f;
        public float scale = 1.0f;
        public float rotation = 0.0f;
        public Vector2 origin = Vector2.Zero;

        public void Create(ContentManager c, Point origin)
        {
            this.lightSprite = c.Load<Texture2D>("lightConeNarrow");
            this.lightRect = new Rectangle(origin, new Point(25, 100));
        }
    }
}
