﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Stel.Desktop.src.objects
{
    public class ForegroundObject : Collider
    {
        private Texture2D sprite;
        private Point pos;
        private Vector2 vel;

        public Vector2 Vel { get => vel; set => vel = value; }
        public Point Pos { get => pos; set => pos = value; }
        public Texture2D Sprite { get => sprite; set => sprite = value; }
        public Rectangle BoundingBox { get => boundingBox; set => boundingBox = value; }
        public void MoveBoundingBox(Point p) { this.boundingBox.Location = p; }
        public void VelY(float y) { this.vel.Y = (int)y; }
        public void VelX(float x) { this.vel.X = (int)x; }
    }
}
