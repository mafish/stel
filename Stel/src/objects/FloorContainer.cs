﻿using System;
using System.IO;                    //File handling
using System.Collections.Generic;   //List

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Stel.Desktop.src.objects
{
    public class FloorContainer
    {
        private List<FloorObject> floors = new List<FloorObject>();
        private string biome = "street";
        private Point floorSize;
        private Point screenSize;

        public List<FloorObject> Floors { get => floors; set => floors = value; }

        public FloorContainer(ContentManager c, Point screenDims, string biome, string levelName)
        {
            //read heightmap from file
            string[] lines = File.ReadAllLines("../../../../src/levels/" + levelName + ".txt");
            int[] heightMap = new int[lines.Length];
            for (int i = 0; i < lines.Length; i++)
                heightMap[i] = Convert.ToInt32(lines[i]);

            this.screenSize = screenDims;
            this.biome = biome;
            this.floorSize = new Point(100, 70);
            
            for (int i = 0; i < heightMap.Length; i++)
            {
                Floors.Add(new FloorObject(c, new Point(floorSize.X * i, 600 - heightMap[i]), this.biome));
            }
        }
        public void Update(Point screenCenter)
        {
           
        }

        public void Draw(SpriteBatch s, Point screenCenter)
        {
            foreach (var floor in this.Floors)
                if(Vector2.Distance(floor.BondingBox.Center.ToVector2(), screenCenter.ToVector2()) < this.screenSize.X * 0.75)
                    floor.Draw(s);

        }
    }
}