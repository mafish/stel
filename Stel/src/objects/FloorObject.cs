﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Stel.Desktop.src.objects
{
    public class FloorObject : Collider
    {
        Texture2D sprite;
        string biome = "street";

        public FloorObject() { }
        public FloorObject(ContentManager c, Point pos, string biom)
        {
            this.biome = biom;
            if(this.biome == "street")
                this.sprite = c.Load<Texture2D>("floorStreet");
            this.boundingBox = new Rectangle(pos, new Point(this.sprite.Width, this.sprite.Height));
        }

        public Texture2D Sprite { get => sprite; set => sprite = value; }
        public Rectangle BondingBox { get => boundingBox; set => boundingBox = value; }

        public void Draw(SpriteBatch s)
        {
            s.Draw(this.sprite, this.boundingBox, Color.White);
        }
    }
}
