﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Stel.Desktop.src.objects 
{
    class Player : ForegroundObject
    {
        public LightSource flashlight = new LightSource();

        public bool standing = false;
        public bool blockedRight = false;
        public bool blockedLeft = false;
        private Vector2 velMax = new Vector2(5, 10);



        public Player(ContentManager c, Point pos)
        {
            this.Sprite = c.Load<Texture2D>("rectGreen");
            this.BoundingBox = new Rectangle(pos, new Point(20, 50));
            this.Pos = pos;
            this.Vel = new Vector2(0, 0);

            this.flashlight.Create(c, this.Pos);
            this.flashlight.isOn = true;
            this.flashlight.origin = new Vector2(this.flashlight.lightSprite.Width * 0.5f * this.flashlight.scale, 0);
        }

        public void Input(KeyboardState kb, MouseState ms)
        {
            if (kb.IsKeyDown(Keys.D))
                this.Vel += new Vector2(0.2f, 0.0f);
            else if (kb.IsKeyDown(Keys.A))
                this.Vel += new Vector2(-0.2f, 0.0f);
            else if (this.standing)
                this.VelX(this.Vel.X * 0.75f);

            if (kb.IsKeyDown(Keys.W))
                if (standing)
                    this.VelY(-10);

            MoveFlashlight(ms.X, ms.Y);
        }

        public void Update()
        {
            if(!standing)
                this.Vel += new Vector2(0, 1);
            this.CheckVel();
            this.Pos += this.Vel.ToPoint();
            this.MoveBoundingBox(this.Pos);
            this.flashlight.lightRect.Location = this.Pos;
        }
        public void Draw(SpriteBatch s)
        {
            s.Draw(this.Sprite, this.BoundingBox, Color.White);
        }

        private void MoveFlashlight(int mouseX, int mouseY)
        {
            float distx = mouseX - SCREEN.WIDTH / 2;
            float disty = mouseY - 480;
            float angle = Convert.ToSingle(Math.Atan2(-disty, -distx));
            this.flashlight.rotation = angle + (float) Math.PI / 2;
        }


        public void CheckCollision(List<Collider> list)
        {
            this.standing = false; this.blockedRight = false; this.blockedLeft = false;
            foreach (var obj in list)
            {
                if (this.boundingBox == obj.boundingBox)
                    continue;
                if (this.boundingBox.Intersects(obj.boundingBox))
                {
                    if (this.BoundingBox.Bottom > obj.boundingBox.Top)
                    {
                        if (Math.Abs(this.BoundingBox.Bottom - obj.boundingBox.Bottom) > obj.boundingBox.Height * 0.7f) // May cause problem with small platforms
                        {
                            this.standing = true;
                            this.Pos = new Point(this.Pos.X,
                                                    obj.boundingBox.Top - this.boundingBox.Height + 1);
                            this.VelY(0.0f);
                            continue;
                        }
                    }
                    if (this.boundingBox.Right > obj.boundingBox.Left && this.boundingBox.Right < obj.boundingBox.Right)
                    {
                        this.blockedRight = true;
                        this.Pos = new Point(obj.boundingBox.Left - this.boundingBox.Width + 1,
                                                this.Pos.Y);
                        if (this.Vel.X > 0.1f)
                            this.VelX(0.1f);
                    }
                    else if (this.boundingBox.Left < obj.boundingBox.Right)
                    {
                        this.blockedLeft = true;
                        this.Pos = new Point(obj.boundingBox.Right - 1,
                                                this.Pos.Y);
                        if (this.Vel.X < -0.1f)
                            this.VelX(-0.1f);
                    }
                }
            }
        }

        private void CheckVel()
        {
            float x = Math.Max(Math.Min(this.Vel.X, this.velMax.X), -this.velMax.X);
            float y = Math.Max(Math.Min(this.Vel.Y, this.velMax.Y), -this.velMax.Y);
            this.Vel = new Vector2(x, y);
        }

    }
}
