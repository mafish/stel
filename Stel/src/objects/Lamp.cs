﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Stel.Desktop.src.objects;

namespace Stel.Desktop.src.objects
{
    public class Lamp : LightSource
    {
        Texture2D sprite;
        Rectangle boundingBox;
        bool isBroken = true;
        bool isActive = true;

        public Lamp( ContentManager c, Rectangle rect)
        {
            this.sprite = c.Load<Texture2D>("streetLamp");
            this.lightSprite = c.Load<Texture2D>("lightCone");
            this.boundingBox = rect;
            this.lightRect = rect;
            this.lightRect.X -= 25;
            this.lightRect.Y += 25;
            this.scale = 1.7f;
        }
        public void Update(float lightLevel)
        {
            if (lightLevel > 0.5f)
                this.isActive = true;
            else
                this.isActive = false;
            if (this.isActive)
            {
                this.isOn = true;
                if (this.isBroken)
                {
                    Random r = new Random();
                    if (r.NextDouble() > 0.95)
                        this.isOn = false;
                }
            }
        }
        public void Draw(SpriteBatch s)
        {
            s.Draw(this.sprite, this.boundingBox, Color.White);
        }
    }
}
