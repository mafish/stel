﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Stel.Desktop.src.debug;

namespace Stel.Desktop.src.menu
{
    class Menu
    {
        TestRect r1;
        TestRect r2;
        TestRect r3;

        
        public Menu(ContentManager c, Vector2 pos1, Vector2 pos2, Vector2 pos3)
        {
            r1 = new TestRect(c.Load<Texture2D>("rectEmpty"), pos1);
            r2 = new TestRect(c.Load<Texture2D>("rectGreen"), pos2);
            r3 = new TestRect(c.Load<Texture2D>("rectRed"), pos3);
        }
        public string CheckClicks(Point pos)
        {
            if (r2.getRect().Contains(pos))
                return "level";
            else if (r3.getRect().Contains(pos))
                return "exit";
            else
                return "menu";
        }
        public void Draw(SpriteBatch s)
        {
            s.Draw(r1.getSprite(), r1.getRect(), Color.White);
            s.Draw(r2.getSprite(), r2.getRect(), Color.White);
            s.Draw(r3.getSprite(), r3.getRect(), Color.White);
        }
    }
}
